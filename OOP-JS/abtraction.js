class Animal {
    constructor () {
        if (this.constructor === Animal) {
            throw new Error("this object is abstraction")
        }
    }

    sound () {
        console.log ("will be implement in sub classes");
    }

    walk () {
        console.log ("will be implement in sub classes");
    }

    eat() {
        console.log ("will be implemeny in sub")
    }

}

// sub class
// class implementation
class Landak extends Animal {
    sound() {
        console.log("suara landak");
    }
    walk () {
        console.log("Landak jalan");
    }
    eat () {
        console.log("landak makan")
    }
}

let newLandak = new Landak ();
newLandak.sound();

// sub class
// class implementation
class kucing extends Animal {
    sound() {
        console.log ("suara kucing");
    }
    walk() {
        console.log("kucing berjalan")
    }
    eat() {
        console.log("kucing makan")
    }
}

let newKucing = new kucing ();
newKucing.sound ();
