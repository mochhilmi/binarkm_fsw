class Mobil {
    // Access modifier
    // public => diakses semua orang
    // private => diakses yang manggil object
    // protected => diakses sama di class/object itu sendiri
    // default => public
    // Atribut
    #warna;
    #tipe;
    #ban;

    constructor(warna, tipe, ban) {
        this.#warna = warna;
        this.#tipe = tipe;
        this.#ban = ban;
    }

    // getter sama setter method
    getWarna() {
        return this.#warna;
    }

    setWarna(warna) {
        this.#warna = warna;
    }

    getTipe() {
        return this.#tipe;
    }

    setTipe(tipe) {
        this.#tipe = tipe;
    }

    getBan() {
        return this.#ban;
    }

    setBan(ban) {
        this.#ban = ban;
    }

    // Behaviour/perilaku
    maju() {
        console.log("maju")
    }

    mundur() {
        console.log("mundur");
        console.log(this._ban);
    }

    static stop() {
        console.log("stop");
    }

}

class Avanza extends Mobil {
    constructor(warna, tipe, ban) {
        super(warna, tipe, ban);
    }
}

let mobil1 = new Mobil("Blue", "Matic", 4); // Initialize object
let avanzaNisa = new Avanza("Silver", "Manual", 3);
console.log(avanzaNisa.getBan());
avanzaNisa.setBan(4);
console.log(avanzaNisa.getBan());
mobil1.setWarna("Black");
console.log(mobil1.getWarna());
console.log(mobil1.getTipe());
console.log(mobil1._ban);
mobil1.maju();
mobil1.mundur();
Mobil.stop();