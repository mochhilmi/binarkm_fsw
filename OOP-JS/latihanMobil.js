class Car {
    constructor (warna, jenis) {
        this.warna = warna;
        this.jenis = jenis;
    }

    static maju () {
        console.log("maju")
    }

    static mundur () {
        console.log("mundur")
    }
}

let mobil = new Car ("merah", "lambo");
console.log(mobil.warna);
console.log(mobil.jenis);
Car.maju ();
Car.mundur ();