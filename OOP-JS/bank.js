class Bank {
    constructor () {
        if (this.constructor === Bank) {
            throw new Error("this object is abstraction")
        }
    }

    transfer () {
        console.log ("will be implement in sub classes");
    }

    mutasi () {
        console.log ("will be implement in sub classes");
    }

    cekSaldo() {
        console.log ("will be implemeny in sub")
    }

}

////////////////////////////// subclass
class bankMandiri extends Bank {
    transfer () {
        console.log ("transfer bank mandiri berhasil");
    }

    mutasi () {
        console.log ("surabaya");
    }

    cekSaldo() {
        console.log (600.000)
    }
}

let newbankMandiri = new bankMandiri ();
newbankMandiri.transfer();

//////////////////////////// subclass
class bankBri extends Bank {
    transfer () {
        console.log ("transfer bank BRI gagal");
    }

    mutasi () {
        console.log ("madura");
    }

    cekSaldo() {
        console.log (700.000)
    }
    
}

let newbankBri = new bankBri ();
newbankBri.transfer();

/////////////////////////////// subclass
class bankBca extends Bank {
    transfer () {
        console.log ("transfer bank BCA pending");
    }

    mutasi () {
        console.log ("sidoarjo");
    }

    cekSaldo() {
        console.log (800.000)
    }
}

let newbankBca = new bankBca ();
newbankBca.transfer();

////////////////////////// subclass
class bankJago extends Bank {
    transfer () {
        console.log ("transfer bank JAGO reload");
    }

    mutasi () {
        console.log ("magetan");
    }

    cekSaldo() {
        console.log (60.000)
    }
}

let newbankJago = new bankJago ();
newbankJago.transfer();