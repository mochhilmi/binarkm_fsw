// banyak bentuk
// tekniknya overloading

class kapal {
    maju() {
        console.log();
    }

    mundur () {
        console.log();
    }
}

class kapalFeri extends kapal {
    constructor(){
        super();
    }

    maju(meter){
        console.log(meter)
    }

    mundur(meter){
        console.log(meter)
    }
}

let newkapalFeri = new kapalFeri();
newkapalFeri.maju(100);