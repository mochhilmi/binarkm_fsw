// class oop di js
class Cat {
    // atribut
    nick;
    color;

    constructor (nick, color) {
        this.nick = nick;
        this.color = color;
    }

    // behaviour(perilaku)
    sound() {
        console.log("sound")
    }
    eat() {
        console.log("eat")
    }
    static walk() {
        console.log("walk")
    }
}

// object oop di js
let catMeong = new Cat("jack", "red"); //creat object
console.log(catMeong.nick);
console.log(catMeong.color);

Cat.walk();
catMeong.eat();
