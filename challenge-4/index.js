const express = require('express');
const app = express(); // Initialize server express
const PORT = 8008; // Listen port
const path = require('path');
const PATH_DIR = __dirname + '/public/';

app.use(express.static('public'));

app.get('/ladingPage', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'ladingPage.html'));
});

app.get('/sewaMobil', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'sewaMobil.html'));
});

app.listen(PORT, () => console.log(`server running at localhost:${PORT}`));