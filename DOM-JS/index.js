// Belajar DOM

// nulis ke page browser
document.writeln("Sebentar lagi Puasa")

// mengambil element html by acessor id
console.info(document.getElementById("title"))

// ambil element html by class
console.info(document.getElementsByClassName("sub-title"))

// ambil element html by tag
console.info(document.getElementsByTagName("h1"))

// ambil element html by query selector
console.info(document.querySelector("h1"))

// menambah isi html
document.getElementById("conten").innerHTML = "lorem ipsum";

// ---------------------
function ambilValueDropdown() {
    let element = document.getElementById("country")
    const value = element.options[element.selectedIndex].value;
    const text = element.options[element.selectedIndex].value;

    console.info("Value selected Dropdown : ", value); 
    console.info("Text selected Dropdown : ", text); 
}

// 

document.getElementById("lorem").style.color = 'Blue';

var paragraph = document.createElement("h1");
paragraph.textContent = "Hilmi Belajar";
document.body.append(paragraph);