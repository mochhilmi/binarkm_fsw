const express = require('express');
const app = express(); // Initialize server express
const PORT = 8008; // Listen port

// parsing request body
app.use(express.json());


// Endpoint GET All 
app.get('/api/products', (req, res) => {
    // your code here
    res.json({ message: "All users" });
});

// Endpoint GET Single 
app.get('/api/products/:id', (req, res) => {
    res.json({ message: "By ID" });
});

// Endpoint POST
app.post('/api/products', (req, res) => {
    res.json({ body: req.body });
});

// Endpoint PUT
app.put('/api/products/:id', (req, res) => {
    res.json({ message: "Update users", body : req.body });
});

// Endpoint DELETE
app.delete('/api/products/:id', (req, res) => {
    res.json({ message: "Delete users" });
});

app.listen(PORT, () => console.log(`server running at localhost:${PORT}`));