class bangunDatar{
    constructor(){
        if(this.constructor === bangunDatar){
        throw new Error("This is abstract")
      }
    }
    
    luas(){
        console.log("luas akan diproses")
    }
  
    keliling(){
        console.log("keliling akan diproses")
    }
}
  
// sub class
class Persegi extends bangunDatar{
    constructor(sisi){
        super();
        this.sisi = sisi;
    }
    
    luas(){
        console.log("Luas Persegi adalah "+ this.sisi*this.sisi)
    }
  
    keliling(){
        console.log("Keliling Persegi adalah "+ this.sisi*4)
    }
}

// inisialisasi persegi
let newPersegi = new Persegi(6);
newPersegi.luas()
newPersegi.keliling()
console.log("----------------------------------------")

//sub class
class PersegiPanjang extends bangunDatar{
    constructor(panjang, lebar){
        super();
        this.panjang = panjang;
        this.lebar = lebar;
    }
    luas(){
        console.log("Luas Persegi Panjang adalah " + this.panjang * this.lebar)
    }
  
    keliling(){
        console.log("Keliling Persegi Panjang adalah "+ 2*(this.panjang + this.lebar))
    }
}

// inisialisasi persegi panjang
let newpersegiPanjang = new PersegiPanjang(5,3);
newpersegiPanjang.luas()
newpersegiPanjang.keliling()
console.log("----------------------------------------")
  
  
//sub class segigita
class Segitiga extends bangunDatar{
    constructor(alas, tinggi){
        super();
        this.alas = alas;
        this.tinggi = tinggi;
    }
    
    luas(){
        console.log("Luas Segitiga adalah "+ (0.5* this.alas * this.tinggi))
    }

    // di sini saya menggukan alas sebagai sisinya karena sisi = alas untuk keliling segitiga
    keliling(){
        console.log("Keliling Segitiga adalah "+ this.alas * 3 )
    }
}

// inisialisasi segitiga
let newSegitiga = new Segitiga(6,3);
newSegitiga.luas()
newSegitiga.keliling()
